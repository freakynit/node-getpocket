var PORT = 8080;

var express = require('express');
var bodyParser = require('body-parser');
var url = require('url');

var app = express();

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }))

var router = express.Router();

app.use('/redirect', require('./routes/api/redirect').router);
app.use('/', function(req, res){
    res.json({"success": true});
});

var server = app.listen(PORT, function() {
    console.log("Listening to port %s", server.address().port);
});
