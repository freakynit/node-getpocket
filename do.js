var GetPocket = require('node-getpocket');

var tagsToSearch = ["reference", "important"];

var config = {
	consumer_key: '48913-6904bcb9e7b34ad0307f1441',
    access_token: '0939af8e-e6cd-a2db-5478-3996ad'
};
var pocket = new GetPocket(config);
pocket.refreshConfig(config);

var params = {
	"detailType":"complete"
};
pocket.get(params, function(err, resp) {
    if(err) throw err;
    
    var count = 0;
    var taggedItems = [];
    var tagsToSearchLength = tagsToSearch.length;
    for(var k in resp.list) {
    	var item = resp.list[k];
    	var foundTagsCount = 0;
    	count++;
    	for(var tag in item.tags){
    		if(tagsToSearch.indexOf(tag) > -1) {
    			foundTagsCount++;
    		}
    	}

    	if(foundTagsCount >= tagsToSearchLength) {
    		taggedItems.push(item.given_url);
    	}
    }
    console.log("Number of total items = " + count);
    console.log("Number of filtered with tags items = " + taggedItems.length);
    console.log("Filtered with tags items :\n"/* + JSON.stringify(taggedItems)*/);
    taggedItems.forEach(function(ele, idx){
    	console.log(ele);
    });
});
