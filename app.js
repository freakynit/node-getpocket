var GetPocket = require('node-getpocket');


// STEP 1
// *****
// var config = {
//     consumer_key: '48913-6904bcb9e7b34ad0307f1441',
//     redirect_uri: 'http://localhost:8080/redirect'
// };

// var pocket = new GetPocket(config);
// var params = {
//     redirect_uri: config.redirect_uri
// };
// pocket.getRequestToken(params, function(err, resp, body) {
//     if (err) {
//         console.log('Oops; getTokenRequest failed: ' + err);
//     }
//     else {
//         // your request token is in body.code
//         var json = JSON.parse(body);
//         var request_token = json.code;
//         console.log("request_token = " + request_token);
//     }
// });


// STEP 2
// *****
// var config = {
//     consumer_key: '48913-6904bcb9e7b34ad0307f1441',
//     request_token: '1d9c2883-3c25-f53a-eb42-09f9f1',
//     redirect_uri: 'http://localhost:8080/redirect'
// };
// var pocket = new GetPocket(config);
// var url = pocket.getAuthorizeURL(config);
// console.log("authorization URL = " + url + " (open this in browser)");


// STEP 3
// *****
var config = {
    consumer_key: '48913-6904bcb9e7b34ad0307f1441',
    request_token: '1d9c2883-3c25-f53a-eb42-09f9f1',
    redirect_uri: 'http://localhost:8080/redirect'
};
var params = {
    request_token: '1d9c2883-3c25-f53a-eb42-09f9f1'
};
var pocket = new GetPocket(config);
pocket.getAccessToken(params, function(err, resp, body) {
    if (err) {
        console.log('Oops; getTokenRequest failed: ' + err);
    }
    else {
        // your access token is in body.access_token
        var json = JSON.parse(body);
        var access_token = json.access_token;
        console.log("access_token = " + access_token);
    }
});
// 0939af8e-e6cd-a2db-5478-3996ad
