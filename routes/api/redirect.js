var express = require('express');
var router = express.Router();

router.get('/', function(req, res){
	console.log("All query strings: " + JSON.stringify(req.query));
	res.json({"success": true});
});

router.post('/', function(req, res){
	console.log("All post data: " + req.body);
	res.json({"success": true});
});

module.exports.router = router;
